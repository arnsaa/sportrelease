# Changelog

## 0.7.1
- Added a background source with a user-defined FOV relative to the optical axis. It supports single energies or spectra.
- Added support for an uncoated margin on the mirror surface around mirror module ribs
- Added toggle to determine the measuring point of the spacing between ribs
- Added option to filter photons by the index of the plate they passed through
- Changed terminology from "wall" to "rib"
- Fixed simulation slowdown after using a large photon list, even when that source was not in use anymore

## 0.7.0

### Optics
- Analytical calculator and IDL dependencies have been removed. This calculator will become a standalone application
- Use WS button is now hidden for mirror assemblies generated from a plate table

### Results
- Simulation data is now loaded into SPORT using numpy arrays, reducing memory overhead and speeding up some calculations
- all data output now includes ray weights as emitted
- Removed fpgeom and entry plane monitors
- Streamlined the loading process somewhat
- Fixed division by 0 bug when flux or observation time are 0
- Added function to load the latest simulation result
- Added option to automatically load the focal plane result of a loaded resultset
- Added option for a default color map
- Results now remember which color map was used for visualization
- Filter sets can now be saved and loaded
- Included a default filter set for the WFI FOV
- Temporarily removed log plotting since the current implementation is broken, and wasn't working well when it did work.

### Scripts
- Resultcmd now includes effective area
